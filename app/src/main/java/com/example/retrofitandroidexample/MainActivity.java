package com.example.retrofitandroidexample;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private TextView textViewResult;
    private ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Loading Data.. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        textViewResult = findViewById(R.id.text_view_result);

     //   AsyncTaskExample asyncTask=new AsyncTaskExample();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<List<Hero>> call = jsonPlaceHolderApi.getPosts();

        call.enqueue(new Callback<List<Hero>>() {
            @Override
            public void onResponse(Call<List<Hero>> call, Response<List<Hero>> response) {

                if (!response.isSuccessful()) {
                        textViewResult.setText("Code: " + response.code());
                    return;

                }
                pDialog.dismiss();
                List<Hero> posts = response.body();

                for (Hero post : posts) {
                    String content = "";
                    content += "postId: " + post.getPostId() + "\n";
                    content += "id: " + post.getId() + "\n";
                    content += "name: " + post.getName() + "\n";
                    content += "email: " + post.getEmail() + "\n";
                    content += "body: " + post.getBody() + "\n\n";

                    textViewResult.append(content);
                }
            }

            @Override
            public void onFailure(Call<List<Hero>> call, Throwable t) {
                textViewResult.setText(t.getMessage());
                pDialog.dismiss();
            }
        });
    }

}